# DemoWeather

Follow the steps below in order to clone, configure, build and run DemoWeather (application doesn't work on iOS as don't have MacBook to check why)

Running application already deployed here: https://kris-demo-weather.herokuapp.com (<strike>first enterance may take some time as the app is cold and needs to spin up</strike> added sheduled lambda on AWS in order to ping the app every 299 mins so it's alwasy warmed up).

Demo movie here: https://youtu.be/HgKFAaM_H6A

## Used libraries

You need to have installed all the libraries:

`node version 8.11.3`<br/>
`npm version 5.6.0`<br/>
`jdk version 1.8.0_171`<br/>
`maven version 3`<br/>
`git version 2.17.1`

## Github clone

Clone <b>application</b> to your machine. Run `git clone https://github.com/krzst/demo-weather.git` <br/>
Clone <b>tests</b> to your machine. Run `git clone https://github.com/krzst/demo-weather-ui-tests`

## Development server configuration, build and run

Run `npm install -g @angular/cli@6.0.8` in order to install angular package.<br/>
Go to <b>application</b> directory.<br/>
Run `npm install` in order to get all required packages (it will take some time).<br/>
Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`.

## Running Selenium tests

Go to <b>tests</b> directory.<br/>
Run `mvn -Dtest=MainTest test`.<br/>
As a result you will see links to reports for each test.<br/>

<b>Already run tests can be found here:</b>

<a href="https://testingbot.com/tests/3f63909d-be38-4e58-9c92-837b924bb5e3?auth=9eb0479b85f8de5ff8fa2b593be7234b">testIfTemperatureDisplayed</a><br/>
<a href="https://testingbot.com/tests/24fb76ec-0a07-4d00-853e-751e6eb7e73b?auth=ccbcb4d847995114029ab69b3f5bdd7e">testInitCity</a><br/>
<a href="https://testingbot.com/tests/7db6aaa6-3d0c-4cd8-b3ce-d49590d2d183?auth=1863b06d7a18671530afcffe61350b6b">testChangeCity</a><br/>
<a href="https://testingbot.com/tests/334bf798-064f-4172-9893-098267a51057?auth=ae581cba8aee7712982ab81a01fa865f">testPickedDefaultDateShouldBeFirstDayAndFirstHour</a><br/>
<a href="https://testingbot.com/tests/661e4ad0-4d13-471e-be43-1c6ed50a97a6?auth=eacc45a781f3511c30e79a16933eb04f">testPageTitle</a><br/>

## What could be improved if I had more time

<ul>
  <li>More integration tests</li>
  <li>Add option to change units (Celsius and Fahrenheit)</li>
  <li>Loader when getting data from OpenWeatherMap</li>
  <li>Caching - easily we can load data only every 3 hours for given location</li>
  <li>iOS and Apple support - don't have MacBook couldn't check why it doesn't work there</li>
  <li>CI/CD integration</li>
  <li>Multilanguage support</li>
</ul>
