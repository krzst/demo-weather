import {Observable} from 'rxjs/index';
import {AfterViewInit, Component, OnInit} from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { DateUtilService} from '../../projects/date-util/src/lib/date-util.service';
import { CommonUtilService} from '../../projects/common-util/src/lib/common-util.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, AfterViewInit {
  weatherDetails: any = {};
  city: any = {};
  nowWeatherDetails: any = {};
  selectedHourDetails: any = {weather: [{description: ''}], main: {}, wind: {}};
  currentDayHoursWeather: any = [];

  selectedDayAndHour: any = new Date();
  currentCity = 'Kielce';

  constructor(private http: HttpClient, public du: DateUtilService, public cu: CommonUtilService) {

  }

  ngOnInit() {
    this.loadWeatherAndUpdate();
  }

  ngAfterViewInit() {

  }

  getWeatherForFiveDays(): Observable<any> {
    return this.http.get<any>(this.getWeatherUrl());
  }

  getWeatherUrl() {
    return 'https://api.openweathermap.org/data/2.5/forecast?q=' + this.currentCity
      + '&units=metric&mode=json&appid=1474b92c8b547daa38d507963ff9a48c';
  }

  loadCityDetails() {
    this.loadWeatherAndUpdate();
  }

  loadWeatherAndUpdate() {
    const thisObj = this;
    const errorInfo = document.getElementById('errorInfo');

    errorInfo.classList.add('visibility-hidden');

    this.getWeatherForFiveDays().subscribe((data) => {

      thisObj.city = data.city;

      thisObj.weatherDetails = {};
      const weatherItems = data.list;

      thisObj.nowWeatherDetails = weatherItems[0];

      thisObj.selectedHourDetails = thisObj.nowWeatherDetails;
      thisObj.selectedDayAndHour = this.du.convertToDate(weatherItems[0].dt_txt);

      weatherItems.forEach(function (element) {
          const hourDetails = element;

          const forecastDate = thisObj.du.convertToDate(hourDetails['dt_txt']);
          const forecastDay = thisObj.du.getOnlyDay(forecastDate);

          if (thisObj.weatherDetails[forecastDay] === undefined || thisObj.weatherDetails[forecastDay] == null) {
            thisObj.weatherDetails[forecastDay] = new Array();
          }

          thisObj.weatherDetails[forecastDay].push(hourDetails);
      });

      thisObj.currentDayHoursWeather = thisObj.weatherDetails[thisObj.cu.objectKeys(thisObj.weatherDetails)[0]];
      thisObj.currentCity = '';

    }, (err) => {
      errorInfo.classList.remove('visibility-hidden');
      thisObj.currentCity = '';
    });
  }

  changeCurrentDay(day) {
    this.currentDayHoursWeather = this.weatherDetails[day];
    this.selectedHourDetails = this.weatherDetails[day][0];

    this.selectedDayAndHour = this.du.convertToDate(this.weatherDetails[day][0].dt_txt);
  }

  changeCurrentHour(date) {
    const thisObj = this;

    const dateObj = this.du.convertToDate(date);
    const day = this.du.getOnlyDay(dateObj);
    const hour = dateObj.getHours();

    thisObj.selectedDayAndHour = dateObj;

    this.weatherDetails[day].forEach(function (element) {
      const elementHour = thisObj.du.convertToDate(element.dt_txt).getHours();

      if (elementHour === hour) {
        thisObj.selectedHourDetails = element;
        return element;
      }
    });
  }

  onSearchSubmit(event) {
    if (event.key === 'Enter') {
      this.loadCityDetails();
    }
  }
}
