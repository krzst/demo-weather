import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class DateUtilService {

  weekDays: any = [ 'Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];
  months: any = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sept', 'Oct', 'Nov', 'Dec'];

  constructor() { }


  getOnlyDay(date?: Date) {
    const month = date.getMonth();
    const day = date.getDate() ;
    const year = date.getFullYear();

    return year + '-' + month + '-' + day;
  }

  convertToDate(str) {
    return new Date(str);
  }

  getOnlyHour(date?: Date) {
    return date.getHours();
  }

  getOnlyDayFromString(str) {
    return this.getOnlyDay(this.convertToDate(str));
  }

  getOnlyHourFromString(str) {
    return this.getOnlyHour(this.convertToDate(str));
  }

  getWeekDay(date: Date) {
    const weekDayIdx = date.getDay();

    return this.weekDays[weekDayIdx];
  }

  getMonth(date: Date) {
    const monthIdx = date.getMonth();

    return this.months[monthIdx];
  }

  getDayNumber(date: Date) {
    return date.getDate();
  }
}
