import { NgModule } from '@angular/core';
import { DateUtilComponent } from './date-util.component';

@NgModule({
  imports: [
  ],
  declarations: [DateUtilComponent],
  exports: [DateUtilComponent]
})
export class DateUtilModule { }
