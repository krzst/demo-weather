import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CommonUtilService {

  constructor() { }

  objectKeys(obj?) {
    if (obj !== undefined && obj !== null) {
      return Object.keys(obj);
    } else {
      return new Array();
    }
  }

  roundNumber(nr: number) {
    return Math.round(nr);
  }
}
