import { NgModule } from '@angular/core';
import { CommonUtilComponent } from './common-util.component';

@NgModule({
  imports: [
  ],
  declarations: [CommonUtilComponent],
  exports: [CommonUtilComponent]
})
export class CommonUtilModule { }
